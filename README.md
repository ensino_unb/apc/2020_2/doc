# Plano da Disciplina - Algoritmos e Programação de Computadores (113476)

## Professores
* Fabricio Ataides Braz
* Nilton Correia da Silva

## Período
2º Semestre de 2.020

## Turmas
* 11A - Fabricio Ataides Braz
* 12A - Nilton Correia da Silva

## Ementa
Princípios fundamentais de construção de programas. Construção de algoritmos e sua representação em pseudocódigo e linguagens de alto nível. Noções de abstração. Especificação de variáveis e funções. Testes e depuração. Padrões de soluções em programação. Noções de programação estruturada. Identificadores e tipos. Operadores e expressões. Estruturas de controle: condicional e repetição. Entrada e saída de dados. Estruturas de dados estáticas: agregados homogêneos e heterogêneos. Iteração e recursão. Noções de análise de custo e complexidade. Desenvolvimento sistemático e implementação de programas. Estruturação, depuração, testes e documentação de programas. Resolução de problemas. Aplicações em casos reais e questões ambientais.

## Método

Independente do método de ensino, a programação de computador é uma habilidade, cuja apreensão exige experimentos contínuos de exercício dos fundamentos da programação. Várias abordagens servem ao própósito de motivar o aluno a buscar esse conhecimento. Neste semestre decidimos aplicar o método de **aprendizado baseado em projeto**.

Uma das principais mudanças que aprendizado baseado em projeto traz é que o foco sai da instrução, em que o professor em sala de aula instrui o aluno sobre conceitos e ferramentas, para a investigação, em que o aluno é desafiado a pesquisar conceitos, técnicas e ferramentas para conseguir alcançar os objetivos do projeto que ele se comprometeu a desenvolver. A perspectiva do professor muda da instrução, para a tutoria, no que diz respeito ao ensino. A perspectiva do aluno muda de passiva para ativa, no que diz respeito ao aprendizado.

A disciplina prevê um total de 90 horas de formação, a serem distribuídas da seguinte maneira:

| Horas | Atividade                          | Natureza |
|-------|------------------------------|---------|
| 18   | Acolhimento e nivelamento              | Mista |
| 21   | Tutorias                                | Síncrona |
| 38   | Atividades em Grupo                    | Assíncrona |
| 1    | Planejamento                           | Assíncrona |
| 12    | Seminários                           | Síncrona |

Serão constituídos grupos em que o aluno, apoioado por até 20 (vinte) tutores (professores e monitores), percorrerá uma trilha de aprendizagem voltada para a construção de dashboards.

## Ferramentas & Materiais
* [Teams](https://teams.microsoft.com/l/team/19%3ab29fd5b216894a86a8033efae42bcb7b%40thread.tacv2/conversations?groupId=d59cb7be-a0ea-458d-94ea-eeafccc5e38b&tenantId=ec359ba1-630b-4d2b-b833-c8e6d48f8059) - Comunicação e trabalho colaborativo
* [Trello](https://trello.com/b/9abnJY4R/project-based-learning-apc) - gerência do projeto
* [Python](https://www.python.org/) - Linguagem de programação
* [Plotly](https://plotly.com/) - Biblioteca de construção de dashboard
* [Gitlab](https://gitlab.com/ensino_unb/apc/2020_1/doc) - Reposotório de código e coloboração


## Avaliação

Para que o aluno seja aprovado na disciplina ele deve possuir desempenho superior ou igual a 50, correspondente a menção MM. Além disso, seu comparecimento deve ser superior ou igual a 75% dos eventos estabelecidos pela disciplina. 

### Desempenho

A avaliação de desempenho é resultado da avaliação pelos professores do resultado do grupo, dado o projeto, juntamente com a avaliação individual do aluno pelos membros do grupo.

* AGP: avaliação do grupo pelos professores. Esta avaliação acontece de acordo com os marcos estabelecidos no template de projeto do Trello compatilhado. No dia da apresentação será sorteado um membro do grupo que deverá ter a capacidade de responder pelo projeto.

* AIG: avaliação individual pelos membros do grupo.

![equation](https://latex.codecogs.com/gif.latex?%5Cdfrac%7B%7B%5Csqrt%7B%7BAGP_1%2AAIG_1%7D%7D%7D%2B%202%2A%7B%5Csqrt%7B%7BAGP_2%2AAIG_2%7D%7D%7D%2B2%2A%7B%5Csqrt%7B%7BAGP_3%2AAIG_3%7D%7D%7D%7D%7B5%7D)

Serão três encontros avaliativos, observando os seguintes apectos do projeto:
1. Design (peso 1)
2. Desenvolvimento (peso 2)
3. Desenvolvimento (peso 2)

### Cronograma

|Aula | Data | Programação | Natureza |
| --- | --- | --- | --- |
| 1 | 1/2 | Acolhida |  Síncrona  |
| 2 | 2/2 | Nivelamento sobre ferramentas de apoio |  Síncrona  |
| 3 | 4/2 | [Introdução a programação](https://github.com/PenseAllen/PensePython2e/blob/master/docs/01-jornada.md) |  Síncrona  |
| 4 | 8/2 | [Variáveis e expressões](https://github.com/PenseAllen/PensePython2e/blob/master/docs/02-vars-expr-instr.md) |  Síncrona  |
| 5 | 9/2 | [Funções](https://github.com/PenseAllen/PensePython2e/blob/master/docs/03-funcoes.md) |  Síncrona  |
| 6 | 11/2 | [Instalação do Python e interface](https://github.com/PenseAllen/PensePython2e/blob/master/docs/04-caso-interface.md) | Síncrona |
| | 15/2 | Feriado | |
| | 16/2 | Feriado | |
| 7 | 18/2 | [Estrutura de Decisão](https://github.com/PenseAllen/PensePython2e/blob/master/docs/05-cond-recur.md)|  Síncrona  |
| 8 | 22/2 | [Funções com Resultado](https://github.com/PenseAllen/PensePython2e/blob/master/docs/06-funcoes-result.md) |  Síncrona  |
| 9 | 23/2 | [Estrutura de Repetição](https://github.com/PenseAllen/PensePython2e/blob/master/docs/07-iteracao.md) |  Síncrona  |
| 10 | 25/2 | [Strings](https://github.com/PenseAllen/PensePython2e/blob/master/docs/08-strings.md) |  Síncrona  |
| 11 | 1/3 | [Listas](https://github.com/PenseAllen/PensePython2e/blob/master/docs/10-listas.md) |  Síncrona  |
| 12 | 2/3 | Plotly <br/> Brainstorm & Research |  Assíncrona  |
| 13 | 4/3 | Plotly <br/> Brainstorm & Research |  Assíncrona  |
| 14 | 8/3 | Plotly <br/> Design |  Assíncrona  |
| 15 | 9/3 | Plotly <br/> Design |  Assíncrona  |
| 16 | 11/3 | Plotly <br/> Design |  Assíncrona  |
| 17 | 15/3 | Avaliação 1 |  Síncrona  |
| 18 | 16/3 | Avaliação 1 |  Síncrona  |
| 19 | 18/3 | Avaliação 1 |  Síncrona  |
| 20 | 22/3 | Plotly <br/> Desenvolvimento |  Assíncrona  |
| 21 | 23/3 | Plotly <br/> Desenvolvimento |  Assíncrona  |
| 22 | 25/3 | Plotly <br/> Desenvolvimento |  Assíncrona  |
| 23 | 29/3 | Plotly <br/> Desenvolvimento |  Assíncrona  |
| 24 | 30/3 | Plotly <br/> Desenvolvimento |  Assíncrona  |
| 25 | 1/4 | Plotly <br/> Desenvolvimento |  Assíncrona  |
| 26 | 5/4 | Plotly <br/> Desenvolvimento |  Assíncrona  |
| 27 | 6/4 | Plotly <br/> Desenvolvimento |  Assíncrona  |
| 28 | 8/4 | Plotly <br/> Desenvolvimento |  Assíncrona  |
| 29 | 12/4 | Plotly <br/> Desenvolvimento |  Assíncrona  |
| 30 | 13/4 | Dash <br/> Desenvolvimento |  Assíncrona  |
| 31 | 15/4 | Dash <br/> Desenvolvimento |  Assíncrona  |
| 32 | 19/4 | Avaliação 2 |  Síncrona  |
| 33 | 20/4 | Avaliação 2 |  Síncrona  |
| 34 | 22/4 | Avaliação 2 |  Síncrona  |
| 35 | 26/4 | Dash <br/> Desenvolvimento |  Assíncrona  |
| 36 | 27/4 | Dash <br/> Desenvolvimento |  Assíncrona  |
| 37 | 29/4 | Dash <br/> Desenvolvimento |  Assíncrona  |
| 38 | 3/5 | Dash <br/> Desenvolvimento |  Assíncrona  |
| 39 | 4/5 | Desenvolvimento |  Assíncrona  |
| 40 | 6/5 | Desenvolvimento |  Assíncrona  |
| 41 | 10/5 | Desenvolvimento |  Assíncrona  |
| 42 | 11/5 | Desenvolvimento |  Assíncrona  |
| 43 | 13/5 | Avaliação 3 |  Síncrona  |
| 44 | 17/5 | Avaliação 3 |  Síncrona  |
| 45 | 18/5 | Avaliação 3 |  Síncrona  |

### Comparecimento

As atividades síncronas serão consideradas para contabilizar a presença.

## Dinâmica no Grupo
Os grupos serão formados com a quantidade 9 alunos. Em cada iteração, ou seja, períodos entre os encontros avaliativos, deverão ser designados no grupo o `líder`, responsável por conduzir o time na execução das metas do período, além de reportar aos professores desvios e problemas de seus membros.

### Equilíbrio
Para que o grupo não seja prejudicado por eventuais desvios de membros do seu grupo, os alunos que não alcancarem nota igual ou superior a 5 na AIG serão desprezados do sorteio para apresentação.

### Evasão
Grupos com menos de 5 alunos, terão seus membros distribuidos em outros grupos.

## Referências Bibliográficas

* Básica 
    * [Pense Python](https://github.com/PenseAllen/PensePython2e/tree/master/docs)
    * Cormen, T. et al., Algoritmos: Teoria e Prática. 3a ed., Elsevier - Campus, Rio de Janeiro, 2012 
    * Ziviani, N., Projeto de Algoritmos com implementação em Pascal e C, 3a ed., Cengage Learning, 2010. 
Felleisen, M. et al., How to design programs: an introduction to computing and programming, MIT Press, EUA, 2001. 

* Complementar 
    * Evans, D., Introduction to Computing: explorations in Language, Logic, and Machi nes, CreatSpace, 2011. 
    * Harel, D., Algorithmics: the spirit of computing, Addison-Wesley, 1978. 
    * Manber, U., Introduction to algorithms: a creative approach, Addison-Wesley, 1989. 
    * Kernighan, Brian W; Ritchie, Dennis M.,. C, a linguagem de programacao: Padrao ansi. Rio de janeiro: Campus 
    * Farrer, Harry. Programação estruturada de computadores: algoritmos estruturados. Rio de Janeiro: Guanabara Dois, 2002.
    * [Plotly](https://dash-gallery.plotly.host/Portal)
    * [Portal Brasileiro de Dados Abertos](http://dados.gov.br)
    * [Kaggle](http://kaggle.com/)
    * [Ambiente de Desenvolvimento Python](https://realpython.com/installing-python/)
    * [Python Basics](https://www.learnpython.org)

* Cursos de python
    - Os alunos podem escolher qualquer fonte extra de aprendendizagem para a linguagem Python. Os recursos são enormes. Algumas sugestões iniciais são:
    * https://www.pycursos.com/python-para-zumbis/
    * https://www.youtube.com/playlist?list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn
    * https://www.youtube.com/playlist?list=PLlrxD0HtieHhS8VzuMCfQD4uJ9yne1mE6
    * https://www.youtube.com/watch?v=O2xKiMl-d7Y&list=PL70CUfm2J_8SXFHovpVUbq8lB2JSuUXgk
    * https://solyd.com.br/treinamentos/python-basico/
    * https://wiki.python.org/moin/BeginnersGuide/NonProgrammers
